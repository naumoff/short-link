<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Services\Facades\GenLink;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        //adding me
         $superUser = [
            'name'=>'admin',
            'email'=>'andrey.naumoff@gmail.com',
            'password'=>Hash::make('admin')
         ];

         $userId = \App\User::create($superUser)->id;
         GenLink::create('http://google.com',$userId);

         $this->call(UsersAndLinkagesSeeder::class);
    }
}
