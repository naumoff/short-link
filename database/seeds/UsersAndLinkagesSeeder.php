<?php

use Illuminate\Database\Seeder;

class UsersAndLinkagesSeeder extends Seeder
{
    private $usersNeeded = 500; //will be twice more
    private $maxQtyOfLinksPerUser = 50;


    public function run()
    {
        for($cycle = 0; $cycle < $this->usersNeeded; $cycle++){
            $userID = factory(\App\User::class)->create()->id;
            $linksQty = rand(0, $this->maxQtyOfLinksPerUser);

            $this->generateLinksForUser($userID, $linksQty);
        }
    }

    #region SERVICE METHODS
    private function generateLinksForUser($userID, $linksQty)
    {
        for($cycle = 0; $cycle<$linksQty; $cycle++){
            factory(\App\Linkage::class)->create(['user_id'=>$userID]);
        }
    }
    #endregion
}
