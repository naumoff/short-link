@if($flash = session('success_message'))
    <div class="alert alert-success">
        {{$flash}}
    </div>
@endif