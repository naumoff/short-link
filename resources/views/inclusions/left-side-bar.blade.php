<div class="card">
    <div class="card-header">Menu</div>

    <div class="card-body">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link level" href="/home/linkages">
                    <div class="flex">My Links</div>
                    <span class="badge badge-success">
                        {{$linkages->count()}} {{str_plural( 'link' , $linkages->count())}}
                    </span>
                </a>
            </li>
            <br>
            <li class="nav-item">
                <a href="/home/linkages/create" class="btn btn-block btn-success btn-sm" role="button">Create New Link</a>
            </li>
        </ul>
    </div>
</div>