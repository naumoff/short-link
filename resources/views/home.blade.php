@extends('layouts.app')

@section('content')
<div class="container container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3">
            @include('inclusions.left-side-bar')
        </div>
        <div class="col-md-9">
            @yield('right-block')
        </div>
    </div>
</div>
@endsection
