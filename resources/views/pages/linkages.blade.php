@extends('home')

@section ('right-block')
    <div class="card">
        <div class="card-header">My Linkages</div>

        <div class="card-body">
            @include('inclusions.success-action')
            @include('inclusions.failure-action')
            <h2>My links</h2>
            <table class="table table-striped" >
                <thead>
                <tr>
                    <th>Original link</th>
                    <th>Generated Link</th>
                    <th>Clicks</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($linkages AS $linkage)
                <tr>
                    <td>{{$linkage->original_link}}</td>
                    <td><a href="{{$url}}/{{$linkage->generated_link}}" target="_blank">{{$url}}/{{$linkage->generated_link}}</a></td>
                    <td>{{$linkage->clicks}}</td>
                    <td>
                        <a href="/home/linkages/{{$linkage->generated_link}}/delete" class="btn btn-danger btn-sm" role="button">Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection