@extends('home')

@section ('right-block')
    <div class="card">
        <div class="card-header">My Linkages</div>

        <div class="card-body">
            @include('inclusions.success-action')
            @include('inclusions.failure-action')
            <h2>Create new link</h2>
            <form method="POST" action="{{route('linkages.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="original_link">Original Link:</label>
                    <textarea class="form-control"
                              rows="5"
                              id="original_link"
                              name="original_link"
                              placeholder="Submit original link here"
                    ></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection