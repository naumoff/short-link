@extends('home')

@section ('right-block')
    <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            Hello {{Auth::user()->name}}!<br>
            You have : {{$linkages->count()}} {{str_plural( 'link' , $linkages->count())}} !<br>
            Total clicks on your {{str_plural( 'link' , $linkages->count())}} : {{$clicks}} {{str_plural( 'hit' , $clicks)}} !
        </div>
    </div>
@endsection