<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>'auth','prefix'=>'home'],function(){

        Route::get('/', 'HomeController@index')->name('home');

        Route::resource('linkages', 'LinkagesController', [
            'except' => [ 'destroy','show','edit','update' ]
        ]);

        Route::get('/linkages/{linkage}/delete', 'LinkagesController@destroy');
    }
);

Route::get('/{linkage}', 'RedirectionController@processRequest');





