<?php

namespace App\Providers;

use App\Services\LinkGenerators\CoreLinkGenerator;
use Illuminate\Support\ServiceProvider;

class LinkGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){}

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('GenLink', function(){
            return new CoreLinkGenerator();
        });
    }
}
