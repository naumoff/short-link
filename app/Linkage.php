<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linkage extends Model
{
    protected $fillable = ['user_id','original_link','generated_link'];

    #region MODEL CUSTOMIZATION METHODS
    public function getRouteKeyName()
    {
        return 'generated_link';
    }
    #endergion

    #region MAIN METHODS
    //method that used in views to provide path to single thread
    public function path()
    {
        return '/linkage/'.$this->generated_link;
    }
    #endregion

    #region RELATIONS METHODS
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    #endregion
}
