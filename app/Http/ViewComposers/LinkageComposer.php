<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 3/28/2018
 * Time: 6:55 AM
 */

namespace App\Http\ViewComposers;

use App\Services\Facades\LogRec;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LinkageComposer
{
    private $user;

    public function __construct()
    {
        if(Auth::check()){
            $this->user = Auth::user();
        }else{
            LogRec::alert([
                'process'=>'generating view composers for linkage',
                'error'=>'User is not authorized'
            ]);
            throw new \Exception("Error code 0002");
        }
    }

    public function compose(View $view)
    {
        $linkages = $this->user->linkages()->get();
        $view->with([
            'linkages'=>$linkages,
            'clicks'=>$this->calculateTotalClicksPerUser($linkages)
        ]);
    }

    #region SERVICE METHODS
    private function calculateTotalClicksPerUser($linkages)
    {
        $totalClicks = 0;
        foreach ($linkages AS $linkage){
            $totalClicks += $linkage->clicks;
        }
        return $totalClicks;
    }
    #endregion
}