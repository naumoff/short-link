<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLinkagePost;
use App\Linkage;
use App\Services\Facades\GenLink;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class LinkagesController extends Controller
{

    public function index()
    {
        $url = env('APP_URL');
        return view('pages.linkages')->with('url',$url);
    }

    public function destroy(Linkage $linkage)
    {
        try{
            $linkage->delete();
            session()->flash('success_message', 'Link was deleted!');
        }catch(QueryException $e){
            session()->flash('error_message', 'Link cannot be deleted');
        }
        return redirect()->back();
    }

    public function create()
    {
        return view('pages.create-link-form');
    }

    public function store(StoreLinkagePost $request)
    {
        GenLink::create($request->input('original_link'));
        session()->flash('success_message', 'Link was successfully created!');
        return redirect()->back();
    }
}
