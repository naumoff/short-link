<?php

namespace App\Http\Controllers;

use App\Jobs\IncrementLinkClicks;
use App\Linkage;


class RedirectionController extends Controller
{
    public function processRequest(Linkage $linkage)
    {
        IncrementLinkClicks::dispatch($linkage);
        return redirect($linkage->original_link);
    }
}
