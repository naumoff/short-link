<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 3/27/2018
 * Time: 9:39 PM
 */

namespace App\Services\LinkGenerators;

use App\Linkage;
use App\Services\Facades\LogRec;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class CoreLinkGenerator implements LinkGenerator
{
    private $userID;

    public function create($originalLink, $userID = null)
    {
        $this->userID = $userID;

        $newLinkage = [
            'user_id'=> $this->getUserID(),
            'original_link'=>$originalLink,
            'generated_link'=> $this->getNewLink()
        ];
        $linkage = Linkage::create($newLinkage);

        return $linkage;
    }

    #region SERVICE METHODS
    private function getUserID()
    {
        if($this->userID !== null){

            return $this->userID;

        }elseif(Auth::check()){

            return Auth::user()->id;

        }else{

            LogRec::alert([
                'process'=>'link generation',
                'error'=>'User is not authorized and user_id is not passed to facade'
            ]);
            throw new Exception('Error code 0001');
        }
    }

    private function getNewLink()
    {
        $originalLink = $this->getUserID().uniqid();
        return $originalLink;
    }
    #endregion
}