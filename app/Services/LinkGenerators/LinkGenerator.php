<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 3/27/2018
 * Time: 9:38 PM
 */

namespace App\Services\LinkGenerators;

use App\User;

interface LinkGenerator
{
    // make Linkage Model with saving to DB, returns saved model
    public function create($originalLink, $userID = null);

}