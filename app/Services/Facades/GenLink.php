<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 3/27/2018
 * Time: 9:36 PM
 */

namespace App\Services\Facades;

use Illuminate\Support\Facades\Facade;

class GenLink extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'GenLink';
    }
}