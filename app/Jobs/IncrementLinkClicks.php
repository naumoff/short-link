<?php

namespace App\Jobs;

use App\Linkage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class IncrementLinkClicks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $linkage;

    public function __construct(Linkage $linkage)
    {
        $this->linkage = $linkage;
    }

    public function handle()
    {
        $this->linkage->clicks += 1;
        $this->linkage->save();
    }
}
